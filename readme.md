## Launch the project

You have to install the dependencies

    pip install -r requirements.txt

Run SQL migrations (not necessary because I don't use the database)

    ./manage.py migrate

Start the server

    ./manage.py runserver

## Run unit tests

    ./manage.py test


## How can this project be improved?

Several things could have been implemented to improve the project:
- Better error handling, especially to differentiate between an API access error and an empty movie list.
- Support for API network errors in unit tests. A third-party API network error could block unit tests and impact a continuous deployment process with this basic implementation.
- Use of serializers to check the API response scheme more strictly.
- Implement another cache system more advanced than the default RAM cache system.
- I could have also tested the caching.
- Make stricter tests on rendering (integration testing)
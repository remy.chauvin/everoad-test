import requests


class GhibliAPIWrapper(object):
    """
    Not really useful to make a wrapper for this simple case but it could be for a more complex API.
    For reusability and simplified testing.
    """

    # I could have used environment variable here
    BASE_URL = "https://ghibliapi.herokuapp.com"

    def list_films(self, limit=20):
        url = "{base_url}{endpoint}".format(base_url=self.BASE_URL, endpoint="/films")
        response = requests.get(url)
        return response.json()

    def list_people(self, limit=20):
        url = "{base_url}{endpoint}".format(base_url=self.BASE_URL, endpoint="/people")
        response = requests.get(url)
        return response.json()

    def list_films_with_people(self, limit=20):
        films = self.list_films(limit=250)
        people = self.list_people(limit=250)

        for f in films:
            f["people"] = [p for p in people if f["url"] in p["films"]]

        return films

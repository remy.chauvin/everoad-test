from django.test import TestCase

from . import GhibliAPIWrapper


class GhibliAPITestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.wrapper = GhibliAPIWrapper()

    def test_list_films_with_people(self):
        films = self.wrapper.list_films_with_people()
        self.assertTrue(isinstance(films, list))

        for film in films:
            self.assertTrue(isinstance(film["title"], str))
            self.assertTrue(isinstance(film["people"], list))

from django.conf import settings
from django.test import TestCase, override_settings
from django.urls import reverse


class FilmsListTestCase(TestCase):
    def test_context(self):
        """
        Check that the context contains a list of movies and the data used in the view.
        """
        response = self.client.get(reverse("films-list"))
        films = response.context["films"]

        self.assertTrue(isinstance(films, list))
        for film in films:
            self.assertTrue(isinstance(film["title"], str))
            self.assertTrue(isinstance(film["people"], list))

    @override_settings(CACHES=settings.TEST_CACHES)
    def test_rendering(self):
        """
        Basic verification that the rendering contains no errors and uses the requested template.
        """
        url = reverse("films-list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "list_films.html")

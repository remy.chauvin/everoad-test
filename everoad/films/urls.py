from django.urls import path

from .views import FilmsListView

urlpatterns = [
    path("", FilmsListView.as_view(), name="films-list"),
]

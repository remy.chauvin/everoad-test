from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView

from .services import GhibliAPIWrapper


@method_decorator(cache_page(60), name="dispatch")
class FilmsListView(TemplateView):
    template_name = "list_films.html"
    http_method_names = ("get", "options")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            wrapper = GhibliAPIWrapper()
            films = wrapper.list_films_with_people()
            context["films"] = films

        except Exception:
            # Here we could implement more advanced error handling.
            pass

        return context
